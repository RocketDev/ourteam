<?php
/**
 * 2018 Native Web
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please contact us for extra customization service at an affordable price
 *
 *  @author Native Web <contact@nativeweb.fr>
 *  @copyright  2018 Native Web
 *  @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of Native Web
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

include_once(_PS_MODULE_DIR_.'ourteam/classes/ourteam_class.php');

class AdminOurteamController extends ModuleAdminController
{
    public function __construct()
    {
        //set variables
        $this->name = "ourteam";
        $this->tab = 'front_office_features';
        $this->table = 'ourteamUser';

        $this->className = 'OurteamUser';
        $this->fields_list = array(
            'id_ourteamUser' => array('title' => $this->l('Id')),
            'name_user_team' => array('title' => $this->l('Name')),
            'poste_user_team' => array('title' => $this->l('Poste')),
            'order_user_team' => array('title' => $this->l('Order')),
        );

        $this->addRowAction('view');
        $this->addRowAction('delete');
        $this->addRowAction('edit');

        //Enable boostrap
        $this->bootstrap = true;

        // Call of the parent constructor method
        parent::__construct();

        // Set fields form for form view
        $this->context = Context::getContext();
        $this->context->controller = $this;

        $this->fields_form = array(
            'name' => array(
                'search' => false
            ),
            'tinymce' => true,
            'legend' => array(
                'title' => $this->l('Add / Edit User'),
            ),
            'input' => array(
                array('type' => 'text', 'label' => $this->l('Name'), 'name' =>
                    'name_user_team', 'size' => 30, 'required' => true),
                array('type' => 'text', 'label' => $this->l('Poste'), 'name' =>
                    'poste_user_team', 'size' => 30, 'required' => true),
                array('type' => 'text', 'label' => $this->l('Order'), 'name' =>
                    'order_user_team', 'value' => 1,  'required' => true),
                array('type' => 'file', 'label' => $this->l('Image'), 'name' =>
                    'image_user_team', 'size' => 30, 'required' => true),
                array('type' => 'textarea', 'label' => $this->l('Description'),
                    'name' => 'description_user_team', 'row' => 34, 'required' => false),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'name' => 'saveOurteamUser')
        );
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'saveOurteamUser';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
    }


    /**
     *
     */
    public function postProcess()
    {
        $urlImage = _PS_MODULE_DIR_.'ourteam/views/img/';

        /* SUPPRESION UTILISATEUR */
        if (Tools::isSubmit('deleteourteamUser')) {
            $id_ourteamUser = (int)Tools::getValue('id_ourteamUser');
            $deleteUser = new OurteamUser($id_ourteamUser);
            @unlink($urlImage.$deleteUser->image_user_team);
            $deleteUser->delete();
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminOurteam', true).'&conf=1');
        }
        /* AJOUTE OU EDIT UN UTILISATEUR */
        if (Tools::isSubmit('saveOurteamUser')) {
            /* EDITION UTILISATEUR */
            if ((int)Tools::getValue('id_ourteamUser')) {
                if ((int)Tools::getValue('order_user_team')) {
                    $id_ourteamUser = (int)Tools::getValue('id_ourteamUser');
                    $updateUser = new OurteamUser($id_ourteamUser);
                    $updateUser->order_user_team = (int)Tools::getValue('order_user_team');
                    //recovers the current url to the picture
                    $oldimgurl = $urlImage . $updateUser->image_user_team;
                    //save the current name of the picture
                    $oldimgname = $updateUser->image_user_team;
                    $name = $this->uploadImageUser($urlImage, $oldimgurl);
                    //We verify if the image is change or not
                    if (isset($name)) {
                        $updateUser->image_user_team = $name;
                    } else {
                        $updateUser->image_user_team = $oldimgname;
                    }
                    //we verify if the name is change or not
                    $userName = $updateUser->name_user_team;
                    if (Tools::getValue('name_user_team') != '') {
                        $updateUser->name_user_team = Tools::getValue('name_user_team');
                    } else {
                        $updateUser->name_user_team = $userName;
                    }
                    //we verify if the poste is change or not
                    $poste = $updateUser->poste_user_team;
                    if (Tools::getValue('poste_user_team') != '') {
                        $updateUser->poste_user_team = Tools::getValue('poste_user_team');
                    } else {
                        $updateUser->name_user_team = $userName;
                    }
                    //we verify if the description is change or not
                    $description = $updateUser->description_user_team;
                    if (Tools::getValue('description_user_team') != '') {
                        $updateUser->description_user_team = Tools::getValue('description_user_team');
                    } else {
                        $updateUser->description_user_team = $description;
                    }
                    $updateUser->update();
                    Tools::redirectAdmin($this->context->link->getAdminLink('AdminOurteam', true) . '&conf=4');
                } else {
                    $this->errors = $this->l('Sort order is not valid');
                }
            } /* IF WE ARE NOT ON EDIT MODE, SO WE CREATE ONE */
            else {
                $userName = Tools::getValue('name_user_team');
                if ($userName != '') {
                    $OurteamUser = new OurteamUser();
                    $OurteamUser->name_user_team = Tools::getValue('name_user_team');
                    $OurteamUser->poste_user_team = Tools::getValue('poste_user_team');
                    $OurteamUser->description_user_team = trim(Tools::getValue('description_user_team'), true);
                    $OurteamUser->order_user_team = (int)Tools::getValue('order_user_team', 1);
                    $name = $this->uploadImageUser($urlImage);
                    if (isset($name)) {
                        $OurteamUser->image_user_team =  $name;
                    } else {
                        $this->errors = $this->l('You must upload an image');
                    }
                    $OurteamUser->add();
                    Tools::redirectAdmin($this->context->link->getAdminLink('AdminOurteam', true).'&conf=3');
                } else {
                    $this->errors = $this->l('You must enter a name');
                }
            }
        }
    }

    public function uploadImageUser($urlImage, $oldimgurl = null)
    {
        if (isset($_FILES['image_user_team']['tmp_name']) && isset($_FILES['image_user_team']['name']) && $_FILES['image_user_team']['name']) {
            if ($_FILES['image_user_team']['error'] == 0 && is_uploaded_file($_FILES['image_user_team']['tmp_name'])) {
                $file_name = Tools::getValue('name_user_team').'_'.uniqid().'_'.$_FILES['image_user_team']['name']; //Le nom original du fichier, comme sur le disque du visiteur (exemple : mon_icone.pdf).
                $size = @getimagesize($_FILES['image_user_team']['tmp_name']);
                if ($size[0] > 500) {
                    $width_img = $size[0]/2;
                    $height_img = $size[1]/2;
                } else {
                    $width_img = null;
                    $height_img = null;
                }
                $extension = Tools::strtolower(Tools::substr(strrchr($_FILES['image_user_team']['name'], '.'), 1));
                $temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
                if (in_array($extension, array('jpg', 'gif', 'jpeg', 'png')) && $_FILES['image_user_team']['size'] < 2000000) {
                    if ($error = ImageManager::validateUpload($_FILES['image_user_team'])) {
                        $this->errors[] = $error;
                    } elseif (!$temp_name || !move_uploaded_file($_FILES['image_user_team']['tmp_name'], $temp_name)) {
                        $this->errors[] = $this->l('Can not upload the file');
                    } elseif (!ImageManager::resize($temp_name, $urlImage.$file_name, $width_img, $height_img, $extension)) {
                        $this->errors[] = $this->l('An error occurred during the image upload process.');
                    }
                    if (isset($temp_name)) {
                        @unlink($temp_name);
                        @unlink($urlImage.'fileType');
                        if (isset($oldimgurl)) {
                            @unlink($oldimgurl);
                        }
                    }
                    return $file_name;
                } else {
                    $this->warnings[] = $this->l('An error occurred during image upload. Are you sure its jpg, gif or png file ? Or his size is < 2mo ?');
                }
            }
        }
        return null;
    }

    public function initPageHeaderToolbar()
    {
        if ($this->display == 'view') {
            // Build delete link
            $admin_delete_link = $this->context->link->getAdminLink('AdminOurteam'). '&deleteourteamUser&id_ourteamUser='. (int)Tools::getValue('id_ourteamUser');
            // Add return button to toolbar
            $this->page_header_toolbar_btn['prevent'] = array(
                'href' => $this->context->link->getAdminLink('AdminOurteam'),
                'desc' => $this->l('Back'),
                'icon' => 'process-icon-back',
            );
            // Add delete shortcut button to toolbar
            $this->page_header_toolbar_btn['delete'] = array(
                'href' => $admin_delete_link,
                'desc' => $this->l('Delete'),
                'icon' => 'process-icon-delete',
                'js' => "return confirm('".$this->l('Are you sure you want to delete it ?')."');",
            );
        }
        parent::initPageHeaderToolbar();
    }
    /**
     * Helper displaying warning message(s)
     * @param string|array $error
     * @return string
     */
    public function displayWarning($warning)
    {
        $output = '
        <div class="bootstrap">
        <div class="module_warning alert alert-warning" >
            <button type="button" class="close" data-dismiss="alert">&times;</button>';

        if (is_array($warning)) {
            $output .= '<ul>';
            foreach ($warning as $msg) {
                $output .= '<li>'.$msg.'</li>';
            }
            $output .= '</ul>';
        } else {
            $output .= $warning;
        }
        // Close div openned previously
        $output .= '</div></div>';
        return $output;
    }

    public function renderView()
    {
        $this->context->controller->addJS($this->_path.'views/js/ourteam.js');
        $urlImage = 'modules/ourteam/views/img/';
        $tpl = $this->context->smarty->createTemplate(_PS_MODULE_DIR_.$this->module->name.'/views/templates/admin/view.tpl');
        $tpl->assign(array(
            'user' => $this->object,
            'urlimage' => $urlImage));
        return $tpl->fetch();
    }
    /**
     * Surcharge de la fonction de traduction sur PS 1.7 et supérieur.
     * La fonction globale ne fonctionne pas
     * @param type $string
     * @param type $class
     * @param type $addslashes
     * @param type $htmlentities
     * @return type
     */
    protected function l($string, $class = null, $addslashes = false, $htmlentities = true)
    {
        if (_PS_VERSION_ >= '1.7') {
            return Context::getContext()->getTranslator()->trans($string);
        } else {
            return parent::l($string, $class, $addslashes, $htmlentities);
        }
    }
}
