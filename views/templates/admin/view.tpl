<fieldset>
    <div class="panel">
        <div class="panel-heading">
            <legend><i class="icon-info"></i>
                {l s='Profil User' mod='ourteam'}</legend>
        </div>
        <div class="form-group clearfix">
            <label class="col-lg-3">{l s='ID:' mod='ourtea'}</label>
            <div class="col-lg-9">{$user->id_ourteamUser}</div>
        </div>
        <div class="form-group clearfix">
            <label class="col-lg-3">{l s='Name:' mod='ourteam'}
            </label>
            <div class="col-lg-9">{$user->name_user_team}</div>
        </div>
        <div class="form-group clearfix">
            <label class="col-lg-3">{l s='Description:' mod='ourteam'}
            </label>
            <div class="col-lg-9">{$user->description_user_team}</div>
        </div>
        <div class="form-group clearfix">
            <label class="col-lg-3">{l s='Image:' mod='ourteam'}</label>
            <div class="col-lg-9"><img width="30%" src="../{$urlimage}{$user->image_user_team}" alt="{$user->name_user_team}"></div>
        </div>
    </div>
</fieldset>
