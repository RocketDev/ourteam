{*

* 2018 Native Web
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses.
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please contact us for extra customization service at an affordable price
*
*  @author Native Web <contact@nativeweb.fr>
*  @copyright  2018 Native Web
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of Native Web

*}

<div id="block_ourteam">
    <div class="row">
        <div id="title_ourteam" class="col-lg-12">
            <h4 class="title-iv text-white">{l s='OUR TEAM' mod='ourteam'}</h4>
            <p class="font-italic subtitle text-white">- {l s='Des professionnels passionnés par leur métier' mod='ourteam'} -</p>
            <div id="cd-team" class="row">
                <div class="container">
                    {foreach from=$ourteamUsers item=$user}
                        <div class="ourteam-users {if $ourteamUsers|@count <= 1}col{elseif $ourteamUsers|@count == 2}col-md-6 col-sm-12{elseif $ourteamUsers|@count == 3}col-lg-4 col-md-6 col-sm-12{else}col-lg-3 col-md-4 col-sm-12{/if} card-user">
                            <div class="content_img">
                                <div class="box-path">
                                    <div class="contain-info">
                                        <p><span class="name_ourteam">{$user.name_user_team}</span><br/>
                                            <span class="poste_ourteam">{$user.poste_user_team}</span>
                                        </p>
                                    </div>
                                    <img class="img_ourteam img_normal" src="{$base_dir}/modules/ourteam/views/img/{$user.image_user_team}"/>
                                    <div class="cd-img-overlay">
                                        <p class="text-white">{$user.description_user_team}</p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    {/foreach}
                </div>
                <div class="cd-overlay"></div>
            </div>
        </div>
    </div>
</div>