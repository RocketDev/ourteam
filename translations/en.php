<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{ourteam}prestashop>adminourteamcontroller_49ee3087348e8d44e1feda1917443987'] = 'Nom';
$_MODULE['<{ourteam}prestashop>adminourteamcontroller_ae51188444fea6d86d85068d44e53520'] = 'Poste';
$_MODULE['<{ourteam}prestashop>adminourteamcontroller_a240fa27925a635b08dc28c9e4f9216d'] = 'Ordre';
$_MODULE['<{ourteam}prestashop>adminourteamcontroller_a0f852ef8bfccd86a0e7217d7959e3e8'] = 'Ajouter / Editer';
$_MODULE['<{ourteam}prestashop>adminourteamcontroller_be53a0541a6d36f6ecb879fa2c584b08'] = 'Image';
$_MODULE['<{ourteam}prestashop>adminourteamcontroller_b5a7adde1af5c87d7fd797b6245c2a39'] = 'Description';
$_MODULE['<{ourteam}prestashop>adminourteamcontroller_c9cc8cce247e49bae79f15173ce97354'] = 'Sauvegarder';
$_MODULE['<{ourteam}prestashop>adminourteamcontroller_b20c9df51d0ee567e5e03c5a05ab07d7'] = 'L\'ordre n\'est pas valide';
$_MODULE['<{ourteam}prestashop>adminourteamcontroller_f1631e117913383d03bf2e0b1b4882a6'] = 'Vous devez upload une image';
$_MODULE['<{ourteam}prestashop>adminourteamcontroller_5892c5c99b45866598eebbcdd6055df6'] = 'Vous devez entrer un nom';
$_MODULE['<{ourteam}prestashop>adminourteamcontroller_dc75b70fb639a9b40637265045bebf79'] = 'Impossible d\'upload le fichier';
$_MODULE['<{ourteam}prestashop>adminourteamcontroller_7cc92687130ea12abb80556681538001'] = 'Une erreur est survenu durant l\'upload du fichier';
$_MODULE['<{ourteam}prestashop>adminourteamcontroller_1be359f05bb62c72f207e1b30ac896ba'] = 'Une erreur est survenu durant l\'upload du fichier, avez vous bien utilisé un format adéquat ?';
$_MODULE['<{ourteam}prestashop>adminourteamcontroller_0557fa923dcee4d0f86b1409f5c2167f'] = 'Retour';
$_MODULE['<{ourteam}prestashop>adminourteamcontroller_f2a6c498fb90ee345d997f888fce3b18'] = 'Supprimer';
$_MODULE['<{ourteam}prestashop>adminourteamcontroller_0f7e7f998f845a05f78445fb60ce4ead'] = 'Etes vous sûr de vouloir le supprimer ?';
$_MODULE['<{ourteam}prestashop>home_48082cd448eee76700ffb58d26325266'] = 'Notre équipe';
$_MODULE['<{ourteam}prestashop>home_4e7b6f946c00a7cdbb1c66487d9516f5'] = 'Professionals passionate about their job';
