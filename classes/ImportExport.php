<?php

include_once(_PS_MODULE_DIR_.'ourteam/classes/ourteam_class.php');

class Ourteam_ImportExport extends Module
{
    public	function __construct()
    {
        $this->name = 'ourteam';
        parent::__construct();
    }
    public function getPostAllLanguage($id_post)
    {
        $sql = 'SELECT p.id_post,pl.title,pl.description,pl.short_description,pl.meta_keywords,pl.meta_description,pl.url_alias,l.iso_code FROM '._DB_PREFIX_.'ybc_blog_post p
            LEFT JOIN '._DB_PREFIX_.'ybc_blog_post_lang pl on (p.id_post = pl.id_post)
            LEFT JOIN '._DB_PREFIX_.'lang l on (pl.id_lang=l.id_lang)
            WHERE p.id_post ="'.(int)$id_post.'"
        ';
        return Db::getInstance()->executeS($sql);
    }
    public function getPosts()
    {
        $sql ='SELECT * FROM '._DB_PREFIX_.'ybc_blog_post p,'._DB_PREFIX_.'ybc_blog_post_shop ps WHERE p.id_post=ps.id_post AND ps.id_shop='.(int)Context::getContext()->shop->id;
        return Db::getInstance()->executeS($sql);
    }


}