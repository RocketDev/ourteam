<?php
/**
 * 2018 Native Web
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please contact us for extra customization service at an affordable price
 *
 *  @author Native Web <contact@nativeweb.fr>
 *  @copyright  2018 Native Web
 *  @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of Native Web
 */

if (!defined('_PS_VERSION_')) {
    exit;
}
class OurteamUser extends ObjectModel
{
    public $id_ourteamUser;
    public $name_user_team;
    public $poste_user_team;
    public $order_user_team;
    public $image_user_team;
    public $description_user_team;
    public static $definition = array(
        'table' => 'ourteamUser',
        'primary' => 'id_ourteamUser',
        'fields' => array(
            'image_user_team' => array('type' => self::TYPE_STRING, 'size' => 500),
            'name_user_team' => array('type' => self::TYPE_STRING, 'required' => true, 'size' => 700),
            'poste_user_team' => array('type' => self::TYPE_STRING, 'required' => true, 'size' => 700),
            'order_user_team' => array('type' => self::TYPE_STRING, 'required' => true, 'size' => 700),
            'description_user_team' => array('type' => self::TYPE_STRING, 'size' => 700),
        )
    );
    public function __construct($id_item = null, $id_lang = null, $id_shop = null, Context $context = null)
    {
        parent::__construct($id_item, $id_lang, $id_shop);
        unset($context);
    }
}
