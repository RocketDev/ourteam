<?php
/**
 * 2007-2019 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2019 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

include_once(_PS_MODULE_DIR_.'ourteam/classes/ourteam_class.php');

class Ourteam extends Module
{
    public $configs = array();

    public function __construct()
    {
        //Init
        $this->name = 'ourteam';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Native Web';
        $this->need_instance = 0;
        $this->secure_key = Tools::encrypt($this->name);
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('Our Team');
        $this->description = $this->l('The best module to promote your team on your homepage');
        $this->ps_versions_compliancy = array('min' => '1.7.0.0', 'max' => _PS_VERSION_);
    }
    /**
     * @see Module::install()
     */
    public function install()
    {
        return parent::install()
        && $this->registerHook('displayHome')
        && $this->_installDb()
        && $this->installTab('IMPROVE', 'AdminOurteam', 'Ourteam');
    }
    /**
     * @see Module::uninstall();
     */
    public function uninstall()
    {
        return parent::uninstall() && $this->_uninstallDb() && $this->uninstallTab('AdminOurteam');
    }

    private function _installDb()
    {
        $languages = Language::getLanguages(false);
        if ($this->configs) {
            foreach ($this->configs as $key => $config) {
                if (isset($config['lang']) && $config['lang']) {
                    $values = array();
                    foreach ($languages as $lang) {
                        $values[$lang['id_lang']] = isset($config['default']) ? $config['default'] : '';
                    }
                    Configuration::updateValue($key, $values);
                } else {
                    Configuration::updateValue($key, isset($config['default']) ? $config['default'] : '');
                }
            }
        }
        //Install db structure
        require_once(dirname(__FILE__).'/install/sql.php');
        return true;
    }

    private function _uninstallDb()
    {
        if ($this->configs) {
            foreach ($this->configs as $key => $config) {
                Configuration::deleteByName($key);
            }
            unset($config);
        }
        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'ourteamUser'.'`');
        $files = glob(dirname(__FILE__).'/views/img/*');
        foreach ($files as $file) {
            if (is_file($file) && $file!=dirname(__FILE__).'/views/img/index.php') {
                @unlink($file);
            }
        }
        return true;
    }
    public function installTab($parent, $class_name, $name)
    {
        // Create new admin tab
        $tab = new Tab();
        $tab->id_parent = (int)Tab::getIdFromClassName($parent);
        $tab->name = array();
        foreach (Language::getLanguages(true) as $lang) {
            $tab->name[$lang['id_lang']] = $name;
        }
        $tab->class_name = $class_name;
        $tab->module = $this->name;
        $tab->icon = 'group';
        $tab->active = 1;
        return $tab->add();
    }

    public function uninstallTab($class_name)
    {
        // Retrieve Tab ID
        $id_tab = (int)Tab::getIdFromClassName($class_name);
        // Load tab
        $tab = new Tab((int)$id_tab);
        // Delete it
        return $tab->delete();
    }

    public function getAllOurteam()
    {
        $sql = 'SELECT * FROM `' ._DB_PREFIX_.'ourteamUser'.'`ORDER BY `order_user_team`';
        return Db::getInstance()->executeS($sql);
    }

    public function hookDisplayHome()
    {
        $ourteamUsers = $this->getAllOurteam();
        $this->smarty->assign(array(
            'ourteamUsers' => $ourteamUsers,
            'base_dir' => Context::getContext()->shop->getBaseURL(true)));
        $this->context->controller->addCSS($this->_path.'views/css/ourteam.css', 'all');
        return $this->display(__FILE__, 'home.tpl');
    }
}
